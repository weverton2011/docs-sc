# Página de Criação de um Site Dinâmico

Iremos configurar um servidor Web Dinâmico com Wordpress.
Para isso iremos verificar os serviços ativos com seguinte comando:

    $ ss -tl
    $ ss -tnl

Após a verificação vimos que o servidor não está com a porta HTTPS ativa.
Para isso vamos fazer a instalação do LAMP.

- L - Linux
- A - Apache2
- M - MySQL
- P - PHP

Utilizando a instalação:

    $ apt install mysql-server php libapache2-mod-php php-server

Após as instalações, iremos criar o diretório www-sc em /var/www/

    $ cd /var/www/
    $ mkdir wwww-sc

    $ chown -R $USER:$USER /var/wwww/www-sc/

Entrando no Diretório /etc/apache2/sites-available/:

    $ cd /etc/apache2/sites-available/

Copiando o arquivo 000-deault.conf para o nome www-sc.conf:

    $ cp 000-deault.conf www-sc.conf

Após a copia do arquivo iremos editar ele com o editor nano:

    $ nano www-sc.conf

Colocando os sequintes dados:

    <VirtualHost *:80>
        <Directory /var/www/www-sc/>
                AllowOverride All
        </Directory>
        ServerName www-sc.oulu.ifrn.edu.br
    
        ServerAdmin weverton.araujo@escolar.ifrn.edu.br
        DocumentRoot /var/www/www-sc
    </VirtualHost>
