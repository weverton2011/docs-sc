# Página de Criação de um Site Estático

Iremos configurar um servidor Web com o Apache2 e o Mkdocs.
Para isso iremos verificar os serviços ativos com seguinte comando

    $ ss -tl
    $ ss -tnl

Após a verificação vimos que o servidor não está com a porta HTTP ativa.
Para isso vamos fazer a instalação do servidor apache2 com o seguinte comando:

    $ apt install apache2
    $ apt install mkdocs

Iremos verificar as portas dos serviços ativas com o sequinte comando:

    $ ss -tl
    $ ss -tnl

Para verificar que o Servidor Apache2 está ativo, utilizaremos o seguinte comando:

    $ systemctl status apache2

Após a verificação, iremos editar os arquivos de configurações do servidor apache2 em `/etc/apache2/sites-available/` que tem o arquivo `000-deault.conf`.
Dentro dele iremos copiar e editar sua copia.

Entrando no Diretório `/etc/apache2/sites-available/`:

    $ cd /etc/apache2/sites-available/

Copiando o arquivo `000-deault.conf` para o nome `docs-sc.conf`:

    $ cp 000-deault.conf docs-sc.conf

Após a copia do arquivo iremos editar ele com o editor nano:

    $ nano docs-sc.conf

Colocando os sequintes dados:

    <VirtualHost *:80>
        ServerName www.docs-sc.oulu.ifrn.edu.br
        ServerAdmin weverton.araujo@escolar.ifrn.edu.br
        DocumentRoot /var/www/docs-sc
    </VirtualHost>

Após as edições iremos iniciar um site Mkdocs no diretória meus-sites:

    $ cd meus-sites
    $ mkdocs new docs-sc
    $ cd docs-sc
    $ nano mkdocs.yml

Colocando o seguinte conteuno no arquivo mkdocs.yml

    site_name: Docs Santa Catarina
    site_url: http://www.docs-sc.oulu.ifrn.edu.br
    site_description: Ducumentação do Portal do Estado de Santa Catarina
    site_author: Weverton Araujo
    
    repo_url: https://gitlab.com/weverton2011/docs-sc
    edit_uri: ""
    
    site_dir: /var/www/docs-sc
    
    theme:
      name: material
      language: pt
    
    nav:
      - Home: index.md
      - Nome da Máquina: hostname.md
      - Interface de Rede: interface.md
      - Site Estático: site-estatico.md
      - Site Dinâmino: site-dinamico.md
    
Em seguida iremos publicar o site com o comando:

    $ mkdocs build